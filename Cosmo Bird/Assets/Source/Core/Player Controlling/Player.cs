using UnityEngine;
using UnityEngine.InputSystem;

namespace CosmoBird.Core
{
    [RequireComponent(typeof(Rigidbody))]
    public class Player : MonoBehaviour
    {
        [SerializeField]
        private float _velocity = 1.5f;


        private Rigidbody _rigibBody;


		private void Awake()
		{
            _rigibBody = gameObject.GetComponent<Rigidbody>();
		}

		private void Update()
		{
#if UNITY_EDITOR_WIN
			if (Keyboard.current.spaceKey.wasPressedThisFrame)
				_rigibBody.velocity = Vector3.up * _velocity;
#elif UNITY_ANDROID
            if (Touchscreen.current.press.wasPressedThisFrame)
				_rigibBody.velocity = Vector3.up * _velocity;
#endif

		}
	}
}
